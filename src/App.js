import React from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {HashLink as Link} from 'react-router-hash-link';
import {Provider} from "mobx-react";

import 'antd/dist/antd.css';

import {Login} from "./pages/login/Login";
import {Store} from "./store";
import {Routes} from "./Routes";

const store = new Store();

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <div style={{
          }}>
            <Routes/>
          </div>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
