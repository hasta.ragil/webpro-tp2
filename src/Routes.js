import React from "react";
import { inject, observer } from "mobx-react";
import {Route, Switch, Redirect} from "react-router-dom";

import {Homepage} from "./pages/homepage/Homepage";
import {Login} from "./pages/login/Login";

@inject("store")
@observer
class Routes extends React.Component {

    state = {
    };

    constructor(props) {
        super(props);
        this.store = this.props.store;

    }

    async componentDidMount() {
    }

    render() {
        return <Switch>
          <Route path="/" exact>
            {this.props.store.userType === 'admin' && <Homepage/>}
            {this.props.store.userType !== 'admin' && <Redirect to={{
              pathname: '/login'
            }}/>}
          </Route>
          <Route path="/login" exact>
            <Login/>
          </Route>
        </Switch>;
    }
}

export {Routes}
