import React from "react";
import { Button } from 'antd';
import { inject, observer } from "mobx-react";
import {withRouter} from "react-router-dom";


@withRouter
@inject("store")
@observer
class Homepage extends React.Component {

  state = {};

  constructor(props) {
    super(props);
    // this.store = this.props.store;
  }

  async componentDidMount() {
  }

  render() {
    return <div style={{
      height: '100vh',
      width: '100vw',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }}>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

      }}>
        <div style={{marginBottom: 16}}>You have successfully logged in.</div>

        <Button type="primary" onClick={() => {
          this.props.store.logout();
          this.props.history.push('/');
        }}>Logout</Button></div>
    </div>;
  }
}

export {Homepage};
